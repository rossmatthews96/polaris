APP = {};

APP.ui = {};

APP.ui.main = {

    init: function () {
        // setup functions in here
        this.setGoogleMaps();
    },

    setGoogleMaps: function() {
        var that = this;

        function setMaps() {
            that.icon = {
                url: 'images/map-icon.png',
                size: new google.maps.Size(52, 58),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(30, 60)
            };

            var maps = [
                {
                    options : {
                        draggable: true,
                        center: { lat: 51.4990624, lng: -0.1412262},
                        zoom: 16,
                        scrollwheel: false
                    },
                    id : 'map'
                }
            ];

            $.each(maps, function(index){
                var map = new google.maps.Map(document.getElementById(maps[index].id), maps[index].options);

                new google.maps.Marker({
                    position: maps[index].options.center,
                    map: map,
                    icon: that.icon
                });
            });
        }

        if ($('#map').length) {
            google.maps.event.addDomListener(window, 'load', setMaps);
        }
    }
};