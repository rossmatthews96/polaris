var options = {};
options.user = 'root';
options.port = 8888;
options.site_path = '/Users/ross/Projects/polaris'; // something like /Users/username/sites/mymampsite


// import the gulp node package
var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    compass = require('gulp-compass'),
    //plumber = require('gulp-plumber'),
	browserSync = require('browser-sync'),
	reload = browserSync.reload,
	mamp = require('gulp-mamp');

// start the server
gulp.task('serve', function() {
	browserSync.init({
		server: {
			baseDir: './'
		},
		proxy: 'http://localhost:8888'
	});
});

gulp.task('config', function(cb){
	mamp(options, 'config', cb);
});

gulp.task('start', function(cb){
	mamp(options, 'start', cb);
});

gulp.task('stop', function(cb){
	mamp(options, 'stop', cb);
});

// Styles Task
gulp.task('compass', function() {
	gulp.src('sass/*.scss')

		//.pipe(plumber())
		.pipe(compass({
			config_file: 'config.rb',
			css: 'stylesheets',
			sass: 'sass'//,
			//style: 'compressed'
		}))

		.pipe(gulp.dest('stylesheets/'))
		.pipe(browserSync.reload({stream:true}));
});

// Scripts Task
// Uglifies
gulp.task('scripts', function(){
	// load the files
	gulp.src('javascripts/*.js')

	//.pipe(plumber())
	//.pipe(uglify())
	.pipe(gulp.dest('build/js'))
	.pipe(browserSync.reload({stream:true}));
});

// Reload all Browsers
gulp.task('bs-reload', function () {
	browserSync.reload();
});

// Watch Task
// Watches JS
//gulp.task('watch', function() {
//	gulp.watch('javascripts/*.js', ['scripts']);
//	gulp.watch('sass-2/**/**/*.sass', ['styles']);
//});

gulp.task('default', ['config', 'serve', 'compass', 'scripts', 'start'], function(){
	gulp.watch('sass/**/**/*.scss', ['compass']);
	gulp.watch('javascripts/*.js', ['scripts']);
	gulp.watch('*.html').on('change', browserSync.reload);
});

