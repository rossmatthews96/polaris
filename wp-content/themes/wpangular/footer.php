        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-4 footer-group">
                        <h5><?php the_field('footer_summary_title', 'option'); ?></h5>
                        <?php the_field('footer_summary_text', 'option'); ?>
                        <a href="<?php the_field('footer_summary_link', 'option'); ?>"><strong>READ MORE</strong></a>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-2 right-col">
                            <a href="/" class="logo-footer hidden-mobile" title=""></a>
                            <ul class="p-nav-social nav navbar-nav">
                                <!-- No share link currently
                                <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <i class="fa fa-share-alt"></i>
                                    </a>
                                </li>
                                -->
                                <li class="nav-item">
                                    <a class="nav-link" href="https://twitter.com/PolarisMediaPR">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://www.facebook.com/PolarisMediaAgency?fref=ts">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <!-- No google plus link currently
                                <li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>
                                -->
                            </ul>
                            <a href="/" class="logo-footer hidden-large" title=""></a>
                            <?php if( have_rows('footer_page_links', 'option') ): ?>
                                <ul class="list-unstyled essential-links">

                                    <?php while( have_rows('footer_page_links', 'option') ): the_row(); ?>
                                        <li><a href="<?php the_sub_field('footer_page_link');?>"><?php the_sub_field('footer_page_link_title');?></a></li>
                                    <?php endwhile; ?>

                                    <li>Polaris Media &copy; <?php echo date('Y'); ?></li>
                                </ul>
                            <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
