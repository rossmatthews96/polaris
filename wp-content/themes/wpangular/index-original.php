<!DOCTYPE html>
<html ng-app="polarisApp">
<head>
    <base href="/wordpress/">
    <title>WP Angular theme</title>
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <h1><a href="<?php echo site_url();?>">Angular JS Demo Theme</a></h1>
    </header>
        <p>My angular theme</p>
        <div ng-view></div>
    <footer>
        &copy; <?php echo date('Y'); ?>
    </footer>
</body>
</html>