app.controller('HomeController', [ '$scope', 'Posts', 'Page', 'Tweets', function($scope, Posts, Page, Tweets) {
    $scope.tweets = [];
    $scope.currentPage = 1;

    // render everything
    $scope.init = function () {
        $scope.renderPage();
        $scope.renderPosts();
        $scope.renderTweets();
    };

    $scope.renderPage = function () {
        var homePageQuery = Page.getPage({'pageSlug': 'home'});
        homePageQuery.$promise.then(function (data) {
            $scope.page = data[0];
            $scope.people = $scope.page.acf.about_people;
            $scope.map_coords = {};
            $scope.map_coords.latitude = $scope.page.acf.map_company_latitude;
            $scope.map_coords.longitude = $scope.page.acf.map_company_longitude;
        });
    };

    $scope.renderPosts = function () {
        var query = Posts.getPosts({
            'postsPerPage': 9,
            'categoryName': 'homepage',
            'page': $scope.currentPage
        });
        query.$promise.then(function(data){
            $scope.posts = data;
        });
    };

    $scope.renderTweets = function () {
        var i = 0;
        var now = moment();

        // use promises to wait for the tweets to come through
        Tweets.async().then(function(response){

            if (response && response.data) {

                // filter our data
                var filtered = _.filter(response.data, function(tweet) {
                    return !tweet.in_reply_to_user_id;
                });

                // now add it to the array of tweets to display
                for (i;i<6;i++) {

                    // if difference in date from now is more than a day then display date
                    if (filtered[i] && filtered[i].created_at && now.diff(moment(filtered[i].created_at), 'days') > 0) {
                        filtered[i].formatted_time = moment(filtered[i].created_at).format('D MMM');

                    } else if (filtered[i] && filtered[i].created_at) {
                        filtered[i].formatted_time = moment(filtered[i].created_at).fromNow();
                    }

                    $scope.tweets.push(filtered[i]);
                }
            }

        }, function(response){
            console.log(response);
        });
    };

    $scope.togglePanel = function (e) {
        if ($(e.currentTarget) && $(e.currentTarget).hasClass('tapped')) {
            $(e.currentTarget).removeClass('tapped');
        } else {
            $(e.currentTarget).addClass('tapped');
        }
    };

    $scope.toggleService = function (e) {

        if ($(e.currentTarget) && $(e.currentTarget).next().is(':visible')) {
            $(e.currentTarget).next().slideUp();
            $(e.currentTarget).removeClass('active');

        } else {
            $(e.currentTarget).next().slideDown(function () {
                $(e.currentTarget).addClass('active');
            });

        }
    };

    $scope.init();
}]);