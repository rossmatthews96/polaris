app.controller('MainController', [ '$scope', '$http', function($scope, $http) {
    $scope.init = function () {
        $scope.renderMenuOnLoad();
        $scope.setMobileNav();
    };

    $scope.renderMenuOnLoad = function () {
        $http.get('wp-json/wp/v2/menus/4').then(function(response){
            console.log('Successfully retrieved menus:');
            console.log(response.data.items);
            $scope.menuItems = response.data.items;

        }, function(response){
            console.log('Error retrieving menus');
            console.log(response);
        });
    };

    $scope.setMobileNav = function () {
        var $mobileNavBtn = $('.js-mobile-nav-btn');
        var $mobileNav = $('.js-mobile-nav');

        $mobileNavBtn.click(function () {
            if (!$mobileNav.hasClass('open')) {
                $mobileNav.addClass('open');

            } else {
                $mobileNav.removeClass('open');
            }
        });
    };

    $scope.closeMenu = function () {
        var $mobileNav = $('.js-mobile-nav');
        $mobileNav.removeClass('open');
    }

    $scope.init();
}]);