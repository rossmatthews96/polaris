app.controller('BlogController', [ '$scope', 'Posts', 'Categories', '$http', function($scope, Posts, Categories, $http) {
    // dom elements
    $scope.loader = $('.js-loader');
    $scope.postsLoader = $('.js-posts-loader-container');
    $scope.page = $('.js-page');

    // set vars
    $scope.currentPage = 1;
    $scope.morePosts = true;
    $scope.currentCategory = 'blog-2';

    // render everything
    $scope.init = function () {
        $scope.renderPostsOnLoad();
        $scope.renderCategories();
        $scope.setInfiniteScroll();
    };

    $scope.renderPostsOnLoad = function () {
        var query = Posts.getPosts({
            'postsPerPage': 9,
            'categoryName': $scope.currentCategory,
            'page': $scope.currentPage
        });
        query.$promise.then(function(data){
            $scope.currentPage++;
            $scope.posts = data;

            $scope.popularPosts = _.filter(data, function(post){
                return post.acf.article_popular;
            });

            $scope.loader.fadeOut('fast', function () {
                $scope.page.fadeIn('fast');
            });
        });
    };

    $scope.renderCategories = function () {
        Categories.async().then(function(response){
            if (response && response.data) {
                var filtered = _.filter(response.data, function(category){
                    return category.name !== 'Frontpage' && category.name !== 'Homepage' && category.name !== 'Uncategorized';
                });

                $scope.categories = filtered;
            }
        });
    };

    $scope.setInfiniteScroll = function () {
        var offset = 200;
        var debouncedLoadMore = _.debounce($scope.loadMore, 750, true);

        $(window).on('scroll', function() {
            if ($('body').scrollTop() >= ($(document).height() - $(window).height()) - offset) {
                debouncedLoadMore();
            }
        });
    };

    $scope.renderMorePosts = function () {
        $scope.postsLoader.fadeIn();
        var query = Posts.getPosts({
            'postsPerPage': 9,
            'categoryName': $scope.currentCategory,
            'page': $scope.currentPage
        });

        query.$promise.then(function(data){
            $scope.postsLoader.fadeOut('fast');
            if (!data.length) {
                $scope.morePosts = false;
            } else {
                $scope.currentPage++;
                $scope.posts = $scope.posts.concat(data);

                if (data.length < 9) {
                    $scope.morePosts = false;
                }
            }
        });
    };

    $scope.loadMore = function () {
        if ($scope.morePosts) {
            $scope.renderMorePosts();
        }
    };

    $scope.filterPostsByCategory = function () {
        if (this.category.slug !== $scope.currentCategory) {
            $scope.currentCategory = this.category.slug;
            $scope.posts = [];
            $scope.morePosts = true;
            $scope.currentPage = 1;
            $scope.renderMorePosts();
        }
    };

    $scope.init();
}]);