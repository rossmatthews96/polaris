app.controller('SearchController', [ '$scope', 'Search', '$http', '$routeParams', function($scope, Search, $http, $routeParams) {
    $scope.loader = $('.js-loader');
    $scope.resultsContainer = $('.js-results');

    // render everything
    (function render() {
        $scope.searchTerm = $routeParams.slug;
        renderSearchResults();
    }());

    function renderSearchResults() {
        var query = Search.getSearchResults({
            'searchTerm': $scope.searchTerm
        });
        query.$promise.then(function(data){
            console.log(data);
            $scope.results = data;

            $scope.loader.fadeOut('fast', function () {
                $scope.resultsContainer.fadeIn('fast');
            });
        });
    }
}]);