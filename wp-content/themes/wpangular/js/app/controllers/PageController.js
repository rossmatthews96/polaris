app.controller('PageController', [ '$scope', '$routeParams', 'Page', function($scope, $routeParams, Page) {
    $scope.loader = $('.js-loader');
    $scope.page = $('.js-page');

    // render everything
    (function render(){
        renderPage();
    }());

    function renderPage(){
        var generalPageQuery = Page.getPage({'pageSlug': $routeParams.slug});
        generalPageQuery.$promise.then(function (data) {
            $scope.content = data[0];

            $scope.loader.fadeOut('fast', function () {
                $scope.page.fadeIn('fast');
            });
        }, function(){
            $scope.loader.fadeOut('fast', function () {
                $scope.page.fadeIn('fast');
            });
        });
    }
}]);