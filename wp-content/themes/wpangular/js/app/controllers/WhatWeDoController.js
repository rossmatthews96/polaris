app.controller('WhatWeDoController', [ '$scope', 'Page', function($scope, Page) {
    $scope.loader = $('.js-loader');
    $scope.page = $('.js-page');
    $scope.panelColors = ['white','blue','blue-dark'];

    // render everything
    (function render(){
        renderPage();
    }());

    function renderPage(){
        var WhatWeDoPageQuery = Page.getPage({'pageSlug': 'what-we-do'});
        WhatWeDoPageQuery.$promise.then(function (data) {
            $scope.groups = data[0].acf.work_groups;

            angular.forEach($scope.groups, function(group) {
                group.panel_color = $scope.panelColors[Math.floor(Math.random() * 3)];
            });

            $scope.loader.fadeOut('fast', function () {
                $scope.page.fadeIn('fast');
            });
        });
    }
}]);