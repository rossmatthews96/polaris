app.controller('AboutController', [ '$scope', 'Page', function($scope, Page) {
    $scope.loader = $('.js-loader');
    $scope.page = $('.js-page');

    // render everything
    (function render(){
        renderPage();
    }());

    function renderPage(){
        var aboutPageQuery = Page.getPage({'pageSlug': 'about-us'});
        aboutPageQuery.$promise.then(function (data) {
            $scope.about = data[0];

            $scope.loader.fadeOut('fast', function () {
                $scope.page.fadeIn('fast');
            });
        });
    }
}]);