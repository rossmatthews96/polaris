app.controller('ContactController', [ '$scope', 'Page', '$http', function($scope, Page, $http) {
    $scope.loader = $('.js-loader');
    $scope.page = $('.js-page');

    // render everything
    $scope.init = function () {
        $scope.renderPage();
    };

    $scope.renderPage = function () {
        var contactPageQuery = Page.getPage({'pageSlug': 'contact-us'});
        contactPageQuery.$promise.then(function (data) {
            $scope.content = data[0];
            $scope.map_coords = {};
            $scope.map_coords.latitude = $scope.content.acf.map_company_latitude;
            $scope.map_coords.longitude = $scope.content.acf.map_company_longitude;

            //$scope.loader.fadeOut('fast', function () {
            //    $scope.page.fadeIn('fast');
            //});
        });
    };

    /*
    *
    * Currently not being used, to be used when we have a web host that has functionality to send emails.

    $scope.master = {
        name: '',
        email: '',
        content: ''
    };

    $scope.send = function (message) {
        $scope.message = angular.copy(message);

        if ($scope.message.name && $scope.message.email && $scope.message.content) {
            $http.post('wp-content/themes/wpangular/send.php', $scope.message).then(function(){
                console.log('Success: form was sent');
                $scope.reset();

            }, function(response){
                console.log('Error: form was not sent');
                console.log(response.statusText);
            });
        }
    };

    $scope.reset = function () {
        $scope.message = angular.copy($scope.master);
        $scope.sendMessage.$setValidity();
        $scope.sendMessage.$setUntouched();
        $scope.sendMessage.$setPristine();

        // reset the email value see: https://github.com/angular/angular.js/issues/10027
        $('#inputEmail').val('');
        $scope.sendMessage.inputEmail.$viewValue = '';
        $scope.sendMessage.inputEmail.$$rawModelValue = '';
    };

     */

    $scope.init();
}]);