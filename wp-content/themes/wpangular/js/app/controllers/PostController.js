app.controller('PostController', [ '$scope', '$http', '$routeParams', 'Post', 'Posts', function($scope, $http, $routeParams, Post, Posts) {
    $scope.loader = $('.js-loader');
    $scope.page = $('.js-page');
    $scope.isClientsCategory = false;
    $scope.clientsCategoryId = 20;

    // render everything
    (function render(){
        renderPage();
        renderPopularPosts();
    }());

    function renderPage(){
        var query = Post.getPost({'postSlug': $routeParams.slug});
        query.$promise.then(function(data){
            $scope.post = data[0];

            getAuthorData($scope.post.author);

            setBackLink();

            $scope.loader.fadeOut('fast', function () {
                $scope.page.fadeIn('fast');
            });
        });
    }

    function renderPopularPosts(){
        var query = Posts.getPosts({
            'postsPerPage': 9,
            'categoryName': 'blog-2',
            'page': 1
        });
        query.$promise.then(function(data){
            $scope.posts = data;

            $scope.popularPosts = _.filter(data, function(post){
                return post.acf.article_popular;
            });
        });
    }

    function getAuthorData(authorId){

        $http.get('wp-json/wp/v2/users/' + authorId).then(function(response){
            $scope.author = response.data.name;

            }, function(error) {
                console.log(error);
            }
        );
    }

    function setBackLink() {
        if ($scope.post.categories.indexOf($scope.clientsCategoryId) > -1) {
            $scope.isClientsCategory = true;
        }
    }
}]);