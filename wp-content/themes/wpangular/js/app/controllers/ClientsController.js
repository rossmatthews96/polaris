app.controller('ClientsController', [ '$scope', 'Page', function($scope, Page) {
    $scope.loader = $('.js-loader');
    $scope.page = $('.js-page');

    // render everything
    (function render(){
        renderPage();
    }());

    function renderPage(){
        var clientsPageQuery = Page.getPage({'pageSlug': 'clients'});
        clientsPageQuery.$promise.then(function (data) {
            $scope.clients = data[0].acf.clients_fields;

            $scope.loader.fadeOut('fast', function () {
                $scope.page.fadeIn('fast');
            });
        });
    }
}]);