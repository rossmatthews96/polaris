app.factory('Page', ['$resource', function($resource){
    return $resource(
        'wp-json/wp/v2/pages?slug=:pageSlug',
        { pageSlug: "@pageSlug"},
        {'getPage': {
            method: 'GET',
            isArray: true
        }
        }
    );
}]);
