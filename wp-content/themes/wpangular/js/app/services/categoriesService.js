app.factory('Categories', function($http){
    return {
        async: function () {
            return $http.get('wp-json/wp/v2/categories');
        }
    };
});