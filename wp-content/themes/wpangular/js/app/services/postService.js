app.factory('Post', ['$resource', function($resource){
    return $resource(
        'wp-json/wp/v2/posts?slug=:postSlug',
        { postSlug: "@postSlug"},
        {'getPost': {
                method: 'GET',
                isArray: true
            }
        }
    );
}]);
