app.factory('Search', ['$resource', function($resource){
    return $resource(
        'wp-json/wp/v2/posts?filter[s]=:searchTerm',
        {
            searchTerm: '@searchTerm'
        },
        {
            'getSearchResults': {
                method: 'GET',
                isArray: true
            }
        }
    );
}]);