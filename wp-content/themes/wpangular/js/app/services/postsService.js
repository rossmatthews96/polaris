app.factory('Posts', ['$resource', function($resource){
    return $resource(
        'wp-json/wp/v2/posts?filter[order]=DESC[orderBy]=date&filter[posts_per_page]=:postsPerPage&filter[category_name]=:categoryName&page=:page',
        {
            postsPerPage: '@postsPerPage',
            categoryName: '@categoryName',
            page: '@page'
        },
        {
            'getPosts': {
                method: 'GET',
                isArray: true
            }
        }
    );
}]);