app.filter('ellipsis', function() {
    return function (text, length) {
        if (text.length > length) {
            return text.substring(0, length) + "...";
        }
        return text;
    }
});

app.filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);