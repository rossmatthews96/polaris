var app = angular.module('polarisApp', ['ngResource', 'ngRoute', 'ngSanitize'])
    .config(function($routeProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $routeProvider
            .when('/', {
                templateUrl: myLocalized.partials + 'home.html',
                controller: 'HomeController'
            })

            .when('/about-us', {
                templateUrl: myLocalized.partials + 'about.html',
                controller: 'AboutController'
            })

            .when('/what-we-do', {
                templateUrl: myLocalized.partials + 'what-we-do.html',
                controller: 'WhatWeDoController'
            })

            .when('/clients', {
                templateUrl: myLocalized.partials + 'clients.html',
                controller: 'ClientsController'
            })

            .when('/blog', {
                templateUrl: myLocalized.partials + 'blog.html',
                controller: 'BlogController'
            })

            .when('/blog/:slug', {
                templateUrl: myLocalized.partials + 'post.html',
                controller: 'PostController'
            })

            .when('/contact-us', {
                templateUrl: myLocalized.partials + 'contact.html',
                controller: 'ContactController'
            })

            .when('/:slug', {
                templateUrl: myLocalized.partials + 'page.html',
                controller: 'PageController'
            })

            .when('/search/:slug', {
                templateUrl: myLocalized.partials + 'search.html',
                controller: 'SearchController'
            })

            .otherwise({
                redirectTo: '/'
            });
    });

    app.run(["$rootScope","$location", function($rootScope, $location) {
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            if (next.indexOf('search') < 0) {
                $rootScope.$broadcast('page:navigate');
            }
        });
        $rootScope.$on('$locationChangeSuccess', function (event, current, previous) {
            $rootScope.currentPage = current + '/';
        });
        $rootScope.$on('$viewContentLoaded', function(event) {
            window.ga('send', 'pageview', { page: $location.url() });
            console.log('Page view tracked', $location.url());
        });
    }]);



