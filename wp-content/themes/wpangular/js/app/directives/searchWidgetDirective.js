app.directive('searchWidgetDirective', ['$location', '$rootScope', function ($location, $rootScope) {
    return {
        restrict: 'E',
        template: '<div class="search pull-right">' +
                      '<input type="text" placeholder="Search...">' +
                      '<a class="search-magnify">' +
                          '<i class="fa fa-search"></i>' +
                      '</a>' +
                      '<a class="search-clear">' +
                          '<i class="fa fa-close"></i>' +
                      '</a>' +
                   '</div>',

        link: function(scope, elem, attrs) {
            var container = elem.find('.search');
            var searchMagnify = elem.find('.search-magnify');
            var searchClear = elem.find('.search-clear');
            var input = elem.find('input');

            function closeSearch () {
                input.val('');
                container.removeClass('active');
                input.blur();
            }

            searchMagnify.on('click', function(e) {
                e.preventDefault();

                if (container.hasClass('active')) {
                    $rootScope.$apply(function() {
                        $location.path('search/' + input.val());
                    });
                } else {
                    container.addClass('active');
                    input.focus();
                }
            });

            searchClear.on('click', function(e) {
                e.preventDefault();

                closeSearch();
            });

            input.on('keyup', function(e) {
                if (e.keyCode === 13) {
                    $rootScope.$apply(function() {
                        $location.path('search/' + input.val());
                    });
                }
            });

            scope.$on('page:navigate', function() {
                closeSearch();
            });
        }
    }
}]);