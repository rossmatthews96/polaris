app.directive('mapDirective', function(Page){
    return {
        restrict: 'E',
        controller: 'mapDirectiveController',
        template: '<div id="map"></div>',
        link: function (scope, elem, attrs, ctrl){
            scope.$watchCollection('map_coords', function(newValue, oldValue){
                if (newValue) {
                    var lat = parseFloat(newValue.latitude);
                    var lng = parseFloat(newValue.longitude);
                }

                var map;
                var icon;
                var marker;

                if (lat && lng) {
                    icon = {
                        url: '/wp-content/themes/wpangular/images/map-icon.png',
                        size: new google.maps.Size(52, 58),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(30, 60)
                    };

                    map = new google.maps.Map(elem[0], {
                        draggable: true,
                        center: { lat: lat, lng: lng},
                        zoom: 16,
                        scrollwheel: false
                    });

                    marker = new google.maps.Marker({
                        position: map.center,
                        map: map,
                        icon: icon
                    });


                    if (ctrl) {
                        ctrl.init(map);
                    }
                }
            });

        },
        replace: true
    }
});