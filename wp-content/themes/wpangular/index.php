<?php
/**
 * The main template file
 *
 */

get_header(); ?>
<div class="main" ng-view></div>
<?php get_footer(); ?>