<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 */
?>
<!DOCTYPE html>
<html lang="en" ng-app="polarisApp">
    <head>
        <!-- base href required for angular -->
        <base href="/">
        <meta charset="utf-8" />
        <meta http-equiv="x-ua-compatible" content="ie=edge, chrome=1" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Polaris Media</title>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB7y5nxeaRNZFbEvIPHXAKivh8S2AhpQEo&v=3.20"></script>
        <?php wp_head(); ?>
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

          ga('create', 'UA-71331359-1', 'auto');
          ga('send', 'pageview');

        </script>
    </head>
    <body ng-controller="MainController">
        <div class="header">
            <nav class="p-top-bar navbar navbar-default navbar-static-top">
                <div class="container">
                    <div class="navbar-header">
                        <a href="/" class="logo-home pull-left" title=""></a>
                        <search-widget-directive></search-widget-directive>
                        <a class="js-mobile-nav-btn mobile-nav-btn hidden-large pull-right">
                            <i class="fa fa-navicon pull-right"></i>
                        </a>
                    </div>
                </div>
            </nav>
            <nav class="js-mobile-nav mobile-nav p-bar p-main-navbar navbar-light navbar hidden-large">
                <ul class="p-nav-links nav navbar-nav container">
                    <li class="nav-item" ng-repeat="item in menuItems">
                        <a ng-click="closeMenu()" class="nav-link ngClass:{ 'home-link' : item.title === 'Home', 'active' : item.url === currentPage }" href="{{item.title === 'Home' ? '/' : item.url}}">
                            <span>
                                {{item.title}}
                            </span>
                        </a>
                    </li>
                </ul>
                <ul class="p-nav-social pull-right nav navbar-nav hidden-mobile">
                    <!--<li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="fa fa-share-alt"></i>
                        </a>
                    </li>-->
                    <li class="nav-item">
                        <a class="nav-link" href="https://twitter.com/PolarisMediaPR" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="https://www.facebook.com/PolarisMediaAgency?fref=ts" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                    </li>
                    <!--<li class="nav-item">
                        <a class="nav-link" href="#">
                            <i class="fa fa-google-plus"></i>
                        </a>
                    </li>-->
                    <li class="nav-item nav-item-email">
                        <a class="nav-link email-link" href="mailto:alice@polaris-media.co.uk">
                            <i class="fa fa-envelope"></i>
                        </a>
                    </li>
                </ul>

            </nav>
            <nav class="p-bar p-main-navbar navbar-light navbar hidden-mobile">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="p-nav-links nav navbar-nav">
                                <li class="nav-item" ng-repeat="item in menuItems">
                                    <a class="nav-link ngClass:{ 'home-link' : item.title === 'Home', 'active' : item.url === currentPage}" href="{{item.title === 'Home' ? '/' : item.url}}">
                                        <div ng-if="item.title === 'Home'" class="nav-home-container">
                                            <i class="icon-nav-home"></i>
                                        </div>
                                        <span ng-if="item.title !== 'Home'">
                                            {{item.title}}
                                        </span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="p-nav-social pull-right nav navbar-nav">
                                <!--<li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <i class="fa fa-share-alt"></i>
                                    </a>
                                </li>-->
                                <li class="nav-item">
                                    <a class="nav-link" href="https://twitter.com/PolarisMediaPR" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="https://www.facebook.com/PolarisMediaAgency?fref=ts" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </li>
                                <!--<li class="nav-item">
                                    <a class="nav-link" href="#">
                                        <i class="fa fa-google-plus"></i>
                                    </a>
                                </li>-->
                                <li class="nav-item nav-item-email">
                                    <a class="nav-link email-link" href="mailto:alice@polaris-media.co.uk">
                                        <i class="fa fa-envelope"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>