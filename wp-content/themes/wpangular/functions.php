<?php

    // add options page for advanced custom fields
    if( function_exists('acf_add_options_page') ) {


        acf_add_options_page(array(
            'page_title' 	=> 'Theme General Settings',
            'menu_title'	=> 'Theme Settings',
            'menu_slug' 	=> 'theme-general-settings',
            'capability'	=> 'edit_posts',
        ));

        acf_add_options_sub_page(array(
            'page_title' 	=> 'Theme Header Settings',
            'menu_title'	=> 'Header',
            'parent_slug'	=> 'theme-general-settings',
        ));

        acf_add_options_sub_page(array(
            'page_title' 	=> 'Theme Footer Settings',
            'menu_title'	=> 'Footer',
            'parent_slug'	=> 'theme-general-settings',
        ));

    }

    // add custom menus
    add_theme_support( 'menus' );

function my_scripts() {

    // before we do anything enqueue the css
    wp_enqueue_style( 'screen.css', get_stylesheet_directory_uri() . '/stylesheets/screen.css' );

    // jquery
    if (!is_admin()) {
        wp_deregister_script('jquery');
        //wp_register_script('jquery', "http" . ($_SERVER['SERVER_PORT'] == 443 ? "s" : "") . "://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js", false, null);
        wp_register_script('jquery', get_stylesheet_directory_uri() . '/js/lib/jquery-2.1.3.min.js');
    }

    //
    // REGISTER THE MAIN FILES WE NEED TO RUN THE APP

    // angular lib files
    wp_register_script('angularjs', get_stylesheet_directory_uri() . '/js/lib/angular.js');
    wp_register_script('angularjs-route', get_stylesheet_directory_uri() . '/js/lib/angular-route.js');
    wp_register_script('angularjs-sanitize', get_stylesheet_directory_uri() . '/js/lib/angular-sanitize.js');
    wp_register_script('angularjs-resource', get_stylesheet_directory_uri() . '/js/lib/angular-resource.js');

    // other lib files
    wp_register_script('bootstrapjs', get_stylesheet_directory_uri() . '/js/lib/bootstrap.js');
    wp_register_script('bootstrap-sprockets.js', get_stylesheet_directory_uri() . '/js/lib/bootstrap-sprockets.js');
    wp_register_script('momentjs', get_stylesheet_directory_uri() . '/js/lib/moment.min.js');
    wp_register_script('underscorejs', get_stylesheet_directory_uri() . '/js/lib/underscore.js');

    // main app file
    wp_register_script('appjs', get_stylesheet_directory_uri() . '/js/app/app.js');

    wp_enqueue_script('my-scripts', get_stylesheet_directory_uri() . '/js/app/app.js',
        array(
            'jquery',
            'angularjs',
            'angularjs-route',
            'angularjs-sanitize',
            'angularjs-resource',
        )
    );

    // general app files
    wp_register_script('main-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/MainController.js');
    wp_register_script('home-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/HomeController.js');
    wp_register_script('about-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/AboutController.js');
    wp_register_script('map-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/MapController.js');
    wp_register_script('clients-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/ClientsController.js');
    wp_register_script('what-we-do-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/WhatWeDoController.js');
    wp_register_script('blog-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/BlogController.js');
    wp_register_script('post-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/PostController.js');
    wp_register_script('contact-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/ContactController.js');
    wp_register_script('page-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/PageController.js');
    wp_register_script('search-controllerjs', get_stylesheet_directory_uri() . '/js/app/controllers/SearchController.js');
    wp_register_script('filtersjs', get_stylesheet_directory_uri() . '/js/app/filters/filters.js');
    wp_register_script('page-servicejs', get_stylesheet_directory_uri() . '/js/app/services/pageService.js');
    wp_register_script('post-servicejs', get_stylesheet_directory_uri() . '/js/app/services/postService.js');
    wp_register_script('posts-servicejs', get_stylesheet_directory_uri() . '/js/app/services/postsService.js');
    wp_register_script('twitter-servicejs', get_stylesheet_directory_uri() . '/js/app/services/twitterService.js');
    wp_register_script('categories-servicejs', get_stylesheet_directory_uri() . '/js/app/services/categoriesService.js');
    wp_register_script('search-servicejs', get_stylesheet_directory_uri() . '/js/app/services/searchService.js');
    wp_register_script('map-directivejs', get_stylesheet_directory_uri() . '/js/app/directives/mapDirective.js');
    wp_register_script('search-widget-directivejs', get_stylesheet_directory_uri() . '/js/app/directives/searchWidgetDirective.js');

    wp_localize_script(
        'my-scripts',
        'myLocalized',
        array(
            'partials' => trailingslashit( get_template_directory_uri() ) . 'partials/'
        )
    );

    // 3rd party lib files
    wp_enqueue_script('bootstrapjs', 'my-scripts');
    wp_enqueue_script('bootstrap-sprocketsjs', 'my-scripts');
    wp_enqueue_script('momentjs', 'my-scripts');
    wp_enqueue_script('underscorejs', 'my-scripts');
    //wp_enqueue_script('sha1', 'my-scripts');

    // other app files
    wp_enqueue_script('main-controllerjs', 'my-scripts');
    wp_enqueue_script('home-controllerjs', 'my-scripts');
    wp_enqueue_script('about-controllerjs', 'my-scripts');
    wp_enqueue_script('clients-controllerjs', 'my-scripts');
    wp_enqueue_script('what-we-do-controllerjs', 'my-scripts');
    wp_enqueue_script('map-controllerjs', 'my-scripts');
    wp_enqueue_script('blog-controllerjs', 'my-scripts');
    wp_enqueue_script('post-controllerjs', 'my-scripts');
    wp_enqueue_script('contact-controllerjs', 'my-scripts');
    wp_enqueue_script('page-controllerjs', 'my-scripts');
    wp_enqueue_script('search-controllerjs', 'my-scripts');
    wp_enqueue_script('filtersjs', 'my-scripts');
    wp_enqueue_script('page-servicejs', 'my-scripts');
    wp_enqueue_script('post-servicejs', 'my-scripts');
    wp_enqueue_script('posts-servicejs', 'my-scripts');
    wp_enqueue_script('twitter-servicejs', 'my-scripts');
    wp_enqueue_script('categories-servicejs', 'my-scripts');
    wp_enqueue_script('search-servicejs', 'my-scripts');
    wp_enqueue_script('map-directivejs', 'my-scripts');
    wp_enqueue_script('search-widget-directivejs', 'my-scripts');
}

add_action('wp_enqueue_scripts', 'my_scripts');
?>