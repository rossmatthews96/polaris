<?php
	if ($_POST) {

		// Here we get all the information from the fields sent over by the form.
		$name = $_POST['inputName'];
		$email = $_POST['inputEmail'];
		$content = $_POST['inputEmail'];
		$receiverEmail = $_POST['email'];

		$to = $receiverEmail;
		$message = 'FROM: ' .$name. "\r\n" . 'EMAIL: '.$email. "\r\n" . 'CONTENT: ' . $content;

		$subject = 'Polaris Contact Form Submission';

		$headers = 'From: rossmatthews96@gmail.com' . "\r\n";

		$subjectSent = 'Ross and Alice are getting married. RSVP Confirmation';
        $messageSent = 'Dear ' .$name. ',' . "\r\n" .
        "\r\n" .
        'Thank you for sending us your RSVP, you sent the following information: ' . "\r\n" .
         "\r\n" .
         $message. "\r\n" .
        "\r\n" .
        'If any of this is incorrect then you can always fill out the form again or reply to this email.' . "\r\n" .
        "\r\n" .
        'We look forward to seeing you in May! ' . "\r\n" .
        "\r\n" .
        'Lots of love,' . "\r\n" .
        'Ross and Alice';

		mail($to, $subject, $message, $headers); // This method sends the mail to the recipient.
		//mail($email, $subjectSent, $messageSent, $headers); //This method sends the mail to the sender.

		header('Content-Type: application/json');
		echo json_encode(array('status' => 'success','message'=> 'Success, the form data was sent'));

	} else {
		header('Content-Type: application/json');
		echo json_encode(array('status' => 'error','message'=> 'There has been an error'));
	}


?>